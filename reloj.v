`timescale 1us/1ns

module reloj(
	input CLK,
	output LED,
);

	assign USBPU = 0;

	reg [25:0] blink_counter;
	wire [31:0] blink_pattern = 32'b0000_1111_0101_0000_1111_1010_1010_1010;

	always @(posedge CLK) begin
		blink_counter <= blink_counter + 1;
	end

	assign LED = blink_pattern[blink_counter[25:21]];

endmodule
